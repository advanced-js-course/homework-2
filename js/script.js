const books = [
    {
        author: 'Люсі Фолі',
        name: 'Список запрошених',
        price: 70
    },
    {
        author: 'Сюзанна Кларк',
        name: 'Джонатан Стрейндж і м-р Норрелл',
    },
    {
        name: 'Дизайн. Книга для недизайнерів.',
        price: 70
    },
    {
        author: 'Алан Мур',
        name: 'Неономікон',
        price: 70
    },
    {
        author: 'Террі Пратчетт',
        name: 'Рухомі картинки',
        price: 40
    },
    {
        author: 'Анґус Гайленд',
        name: 'Коти в мистецтві',
    }
];

const root = document.getElementById('root');
const booksContainer = document.createElement('ul');


books.forEach((book) => {
    try {
        let errors = []

        if (!book.hasOwnProperty('author')) {
            errors.push('author')
        }

        if (!book.hasOwnProperty('name')) {
            errors.push('name')
        }

        if (!book.hasOwnProperty('price')) {
            errors.push('price')
        }

        if (errors.length > 0) {
            throw new Error(`В книзі відсутнє поле ${errors.join(', ')}`)
        }

        const bookItem = document.createElement('li');
        bookItem.innerHTML = `${book.author} - ${book.name} - ${book.price} грн`;

        booksContainer.appendChild(bookItem);
    } catch (error) {
        console.error(error.message)
    }
});

root.appendChild(booksContainer);
